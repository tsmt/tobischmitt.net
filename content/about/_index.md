---
title: ""
---

## Info

My name is Tobi, I'm in my 30s and live in mid-west germany.

At my day job, I write system software for a local company doing financal analyics. Earlier in C++, currently in Rust. For fun, I [currently do some internet things](/about/now/).

In my free time I like to go cycling (especially Mountain- and Gravelbikes), row regularly on my Waterrower and sometimes play videogames.

What I currently do, you can [read here](/about/now/)
