---
title: "Blogroll"
date: 2023-10-14T22:00:00+02:00
updated: 2024-08-23T22:00:00+02:00
---

## Articles
- [Death by a thousand microservices](https://renegadeotter.com/2023/09/10/death-by-a-thousand-microservices.html)
- [To listen well, get curious](https://www.benkuhn.net/listen/)
- [Attention is your scarcest resource](https://www.benkuhn.net/attention/)
- [You don't need a modal window](https://youdontneedamodalwindow.dev/)
- [Radical simplicity in technology](https://www.radicalsimpli.city/)
- [Just use Postgres for everything](https://www.amazingcto.com/postgres-for-everything/)
- [Recession and crisis in the IT industry](https://trojanczyk.eu/crisis-in-the-it-industry/)
- [Quitting My Job For The Way Of Pain](https://ludic.mataroa.blog/blog/quitting-my-job-for-the-way-of-pain/)

## Blogs

### General
- [Jason Fried](https://world.hey.com/jason) - I like Jasons concepts of how to structure companies and workload. He funded the company behind basecamp together with David Heinemeier Hansson.
- [Ben Kuhn](https://www.benkuhn.net/) - I had an interview with Ben when he was the CTO of [wave](https://wave.com), a finance startup from Senegal. He's a really great and respectful guy and I really appreciate the hour talk that we had. Since then, I regularly check his blog.

### Tech
- [David Heinemeier Hansson](https://world.hey.com/dhh) - 37signals co-owner and also long-year blogger. Some of his takes I like, some I don't. But it's always nice to read differnent opinions as well.
- [Deniz Aksimsek](https://denizaksimsek.com/en/) - A guy who's active in the indieweb and htmx community. Actually, when I bought the [Hypermedia Systems](https://hypermedia.systems/) book he co-authored, I checked his website and discovered the indieweb. So Deniz, thanks a lot 🙇‍♂️.
- [Jonathan Pallant](https://thejpster.org.uk/) - When you do things with Rust and embedded systems, you should have heard about Jonathan.
- [fettblog.eu](https://oida.dev) - I'm confused you can use fettblog.eu and oida.dev. Wondering with how many Domains Stefan can come up.
- [Dirkjan Ochtman](https://dirkjan.ochtman.nl) - Creator of Askama Templating engine
- [Armin Ronacher](https://lucumr.pocoo.org/) - As well as Ben, Armin is a techie under the "general" section as well, because I absolutely love reading his takes on life and work.
- [Nikhil Suresh](https://ludic.mataroa.blog/) - Really cool writing style and great tech articles such as "I Will Fucking Piledrive You If You Mention AI Again"
