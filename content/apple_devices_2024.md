---
title: "On Apple devices"
date: 2024-03-17T11:17:00+02:00
taxonomies:
tags: ["phone", "consumerism", "sustainability", "apple", "repair"]
---

In the last several months, my iPhone 11 battery got worse and worse. I'm not really mad for that, because I think that a reduced battery after 4,5 years constant usage is perfectly fine. Before I bought that phone in late 2019, I was living for a long time in the Android/Windows world. Back then, I was not particularly unhappy, but I wanted to test something different and also wanted something that "just works".

After using Apple throughout the whole pandemic and also switching with most of my devices to apple products I can only say that Apple pretty much fulfills what they promise. That stuff just works. Still, I'm not very happy for several reasons with the Apple ecosystem, which I will explain in this text. Still, I won't dump all my apple products immediately.

## What Goes, What Remains

So, what I've bought in the last years from apple started with an iPhone 11, went on with Airpods Pro, then a Apple Watch Series 6, a used iPad from 2017, and the last thing I bought was a used Macbook Air M1 from 2020. I was quite happy with all of them, nevertheless, some I wouldn't buy again. The main reason is the vendor-lock-in. Let me explain.

### Airpods Pro
Those, I actually did not buy again since they broke literally 2 month after my guarantee ended. As a replacement, I ordered Anker Soundcore Life P3 and I'm quite happy with the battery life and sound, for half the price. Also, the main selling points for Airpods, which is the seamless handover between Apple devices never really worked well for me. I didn't really feel locked in with this device, since they also worked with Windows PCs and Android devices.

### Apple Watch
The Apple Watch is great, but it's just not for me. All the smart features it has I basically never use. I don't use Siri, I deactivated all notifications and uninstalled all apps possible except my sports tracking app Strava. I basically have a very expensive fitness tracker with a lot of additional features I never use, and which I need to recharge every day. And what I want to talk about here especially is vendor locking. If you have such an expensive device and the paired phone breaks, like mine just did, you basically have no choice than buying another iPhone, even if you don't want to. Or you sell you Apple Watch as well which is not a financial desaster, but it feels unnecessary.

So I decided to switch to a [Garmin Instinct 2 Solar](https://www.garmin.com/de-DE/p/775697/pn/010-02627-00). Even though I will keep my iPhone for now. One reason is, that I want to make a decision on a new phone in the upcoming year and I don't want a vendor-lockin to influence my decision. The other reason is that the Instinct 2 is just available at a great price of 250€. On longer bike rides, my Apple Watch actually died multiple times already which is very unfortunate. The Garmin instead, with it's limited display and Solar technology has at least 30 hours battery life on GPS and in theory "unlimited" battery life through it's solar charge feature. This also seems very sustainable to me.

### iPhone 11
Positively I can say, that the phone and software **just worked**. And in a very good way. I very rarely had any software issues, nor did I have any problems with the hardware. Now, when I wanted to replace the battery myself, I had to discover again that I'm just not made for electronics. Even though the instructions on iFixit were pretty clear, I managed to destroy the screen of the phone by cutting the display cables when removing the glue. So that was stupid, and that's on me. So I now ordered another display to fix it as well.

But looking at the reparability of the phone I'm still not very happy. Sure, I could give it to apple and spend 150€ on a new battery or 300€ on a new display. But I think that replacing a battery should not be a task that involves this amount of work. Which by the way also counts for nearly all Android devices these days.

Also, monitoring the behaviour of Apple in the last years I'm not really happy how they act in fights with the EU. In the "charger war", they tried to procrastinate switchting to USB-C for years. There were rumors already on the iPhone 12 to support it, and in the end it came with the 15. Also, Apple shows [childish behaviour](https://www.theverge.com/2024/2/15/24074182/apple-drops-support-iphone-web-apps-eu-dma) by artificially breaking working functionality for EU users as a revenge to the DMA which was enforced on Apple. Apple obviously does not want me to choose the browser I want to use, Apple does not want me to choose the Store I install apps from. And as a revenge, they just break functionality or [held up updates for major apps](https://www.reddit.com/r/apple/comments/1bex0jc/spotify_says_its_iphone_app_updates_in_the_eu_are/). It's fine and good that their products just work and have good build quality, but I'm currently not willing to accept that I as a customer am constantly patronized by Apple.

I'm currently considering two phone brands from the EU. One is Fairphone from netherlands, and the other one is Shift from germany. Actually, when I bought my iPhone11 in 2019 I also considered both of them, I think the Shiftphone 5me and the Fairphone 3. Back then, the cpus, cameras and also displays were not really up to date, but seemed to be years behind the market. With the [Fairphone 4](https://shop.fairphone.com/de/fairphone-4-e-operating-system) and [Shiftphone 6mq](https://shop.shiftphones.com/shift6mq.html), this changed a lot and both of them got really good feeback on what they did. Late 2023, Fairphone released the [Fairphone 5](https://shop.fairphone.com/de/fairphone-5) which also got good feedback. Shiftphone will release it's [Shiftphone 8](https://shop.shiftphones.com/shiftphone-8.html) in August 2024.

My plan currently is to **wait for the Shiftphone 8, and then decide on a new phone**.

### iPad

This device, I rarely use. It's actually always around the sofa and I use it sometimes to surf while chilling around. I actually bought it used from a friend, but I also don't have any issues with posessing it except the things I already mentioned around iOS and the phone. But I also would most likely not switch it.

### Macbook

Honestly, the Macbook I just love. This is the greatest laptop I ever owned. The build quality is awesome and and the M1 ARM processor is crazy fast. Also, the lock-in topics I mentioned on the iPhone are not remotely as bad on the Mac. I can freely choose a browse which is not running WebKit, I can get root access and even install a Linux OS if I like to.

The only thing which I maybe do not like that much is the look&feel of MacOS. I regularly use different computers with different operating systems and to me the different keyboad layout to Linux and Windows is just annoying. I would wish that apple would give users the opportunity to select between different keyboad layouts and also Window-Layouts etc. But I think that's a wish that will never be fulfilled. We're still talking about Apple.

Anyway, that's still not reason for me to get rid of the device by now. I bought it, and I want to keep it for some time longer. I use another Macbook at work for programming, so if I switch my personal one to a linux machine, I still need to cope with the shortcut and keyboard differences all the time.

Still, similar to the Shift- and Fairphones, there's [Framework](https://frame.work), which are building highly repariable computers for end consumers. Maybe, in a few years, when I have the chance to switch my work device, I will ask for a non-mac laptop and also switch to a frame-work as my private device then.

## Conclusion

In conclusion, my experience with Apple products over the years has been a mix of satisfaction and frustration. While I appreciate their reliability and durability, I'm increasingly wary of the limitations and costs associated with the Apple ecosystem. As I look towards the future, I'm inclined to explore alternatives that offer more flexibility and affordability. By gradually transitioning away from Apple, I hope to regain control over my device choices and minimize the influence of vendor lock-in.

- Tobi

