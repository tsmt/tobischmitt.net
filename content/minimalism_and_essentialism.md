---
title: "Essentialism, Minimalism and Capitalism"
description: "Navigate Essentialism and Minimalism, exploring their impact on lifestyle, philosophy, and the nuances of capitalism. A critical examination."
date: 2023-10-17T15:00:00+02:00
draft: false
taxonomies:
  tags: ["minimalism", "essentialism", "asceticism"]
---

In the last years I read a lot about the concepts of essentialism and minimalism and somehow identify with the ideas. But I would like to answer the question for me if I'm a minimalist or essentialist. For a while I'm wondering in how far both concepts can be distinguished. I have my on thoughts on it on how to do that, but I would like to check in this blog entry if I'm right or wrong about it. The generalized statement of both is pretty much the same:

> Have a higher quality in life by doing and owning just what's really important for you.

## General

Minimalism as a lifestyle is often accused to be more or less just a tool of [surveillance or attention capitalism](https://shoshanazuboff.com/book/) to keep people on consuming facebook, instagram and tiktok feeds of the [bigger minimalism influencers](https://www.theminimalists.com/). I've rarely seen minimalists recommending their listeners or readers to just quit the media they are delivering on. Which makes a lot of sense because most of those influencers by now make their money with social media content, so telling your audience to leave your platform can be very counter productive for your personal wallet. On the other hand, you're a minimalist so money is not that important, is it? In fact, sometimes I get the impression that you cannot be a minimalist without having some kind of social media presence, blog or podcast to sell courses and books. And you find just **lots** of it.

On the other hand I rarely find something on the label essentialism, and rarely people identify as "essentialist", at least it seems to be less common than for minimalists. One of my first books I read about those concepts was called "essentialism" by Greg McKeown. A book of which I have mixed feelings about because a huge part of why it tells you to try the essentialism lifestyle is your work. So the main question after reading the book was: what is this **really** about? Living a better life or *being more effective at work*? Yes, please understand this as another critique on capitalism buying and reselling lifestyles. 

Fortunately, essentialism has a much longer track record than minimalism. Philosophically seen, [essentialism](https://en.wikipedia.org/wiki/Essentialism) is the view that every object has a set of attributes that are focal to their identity. This means that the essence is basically a combination of substance and form analog to ideas and forms in Platons idealism. So it's basically a philosophical view that everything can be abstracted to some specific core attributes. Once you replace "everything" with "everyone" or even groups of people, this can easily drift into generalizations and the [danger of racism and fueling identity politics](https://theanarchistlibrary.org/library/lawrence-jarach-essentialism-and-the-problem-of-identity-politics).

## What I think

I think, minimalism is about **things**, and essentialism is about a wholistic view on your mind. Even if the influencer-minimalists would argue differently. Actually, I think that essentialism is, what minimalism want's to be. Why?

Minimalism to me appears as a capitalist lifestyle which does basically identify by having just a few, but very intentional (and luxurious) things. Following minimalist influencers for a while, you will see that they do not own a lot of stuff, buf what they own is usually *ridiculously* expensive and also show off with those a lot. Also, they usually justify each of their belongings by the quality of the product, how it is the best product in it's category and how much it "brings joy" to their lifes. But most of it still seems to be about **things** instead of work, private life etc. And the main reason for this is the big minimalism industry which basically - contrary to what they say - talk about things and stuff over and over.

Essentialism as a lifestyle instead seems to provide what minimalism fails to - by actually delivering **less** content. I actually had a hard time finding online content on essentialism - which makes sense since the essentialists are probably focussing on essential things in their lifes instead of just producing video over video and hosting it on social media platforms which make their money by selling products to the audience. One thing you have to keep in mind when implementing a essentialist view on the world is that the belief that everthing is deducible to some core attributes can also lead to prejudice.

## What ChatGPT thinks

Wouldn't this be interesting?

> Essentialism and minimalism are two distinct concepts, often associated with different aspects of life, philosophy, and design. Here's an overview of each term:

> Essentialism:
> - In Philosophy: Essentialism is a philosophical concept that suggests that entities have inherent, fundamental qualities or characteristics that define their essence. This view holds that things have a set of properties that make them what they are, and these properties are necessary for the existence of the entity.
> - In Education: Essentialism in education advocates for a focus on essential skills and knowledge that are deemed necessary for students. It emphasizes a core curriculum that often includes subjects like mathematics, science, literature, and history.
> - In Personal Development: Essentialism in personal development is a mindset that encourages individuals to identify and focus on what is truly essential in their lives. It involves prioritizing activities and commitments that align with one's values and goals while eliminating non-essential distractions.

> Minimalism:
> - In Art and Design: Minimalism is an art and design movement characterized by simplicity, clean lines, and a focus on essential elements. It involves using the bare minimum of components, such as color, shape, and form, to convey a message or create an aesthetic experience.
> - In Lifestyle: Minimalism as a lifestyle advocates for simplifying one's life by decluttering physical possessions and focusing on experiences rather than material possessions. The goal is to eliminate the excess and only keep what is truly valuable or necessary.
> - In Technology: Minimalism in technology refers to designing products and interfaces with a minimalistic approach, often emphasizing simplicity and ease of use. This can apply to both hardware and software design.

>In summary, while essentialism is a philosophical concept that deals with inherent qualities and the core nature of things, minimalism is often a design or lifestyle choice that emphasizes simplicity and the elimination of excess. Essentialism is more about identifying and focusing on the essential qualities, while minimalism is about reducing to the essential elements or components.

So it seems that ChatGPT comes to an additional conclusion. Also notice, how the points in essentialism are about thoughts and ideas, while for minimalism it's about things: art & design, technology. Both have "lifestyle".
