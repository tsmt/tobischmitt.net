---
title: "On screens and productivity"
date: 2024-03-16T09:30:00+02:00
taxonomies:
  tags: ["desk", "essentialism", "screens", "clutter", "stress"]
---

## Introduction
I know there are several studies which show that [multimonitor setups increase office workers' productivity](https://www.fujitsu.com/de/about/resources/news/press-releases/2021/20210222.html). I myself have often had doubts about those studies. In the linked case, the "study" was based on a survey of ~2000 people working in offices. The productivity was "measured" by self-evaluation of those people. So, you don't know if they were really more productive or if they just felt that way.

Another question I have for this specific study is whether the "second monitor" is actually a real physical monitor with 24"+ added to the prior single monitor, which was the laptop. The press release does not make that clear. To me, it is obvious that if you add a mouse, a keyboard, and a monitor to your laptop, your productivity will increase, even if you stay in single-monitor mode on the larger screen and close the laptop, like I do most of the time.

## Definitions
Let me clarify some terms I will be using in the following text:

- **0.5 screen setup**: The typical laptop. This is fine if you are currently traveling, but I don't see how you can be productive for a longer time just on your laptop screen.
- **1 screen setup**: One larger screen with good ergonomic positioning in front of your face.
- **1.5 screen setup**: One larger screen with the laptop screen.
- **multi-screen setup**: 2+ larger screens.

Basically, I see a normal computer monitor as one screen and a laptop as 0.5. An ultrawide screen, to me, is also like 1.5 monitors.

## Where's the Benefit
For many workloads, I totally see the benefit, especially if you compare things at all levels. Examples include:

- A frontend developer who has the code on one side and the open browser with live-refresh on the other.
- A support engineer who has logs on one side and code on the other.
- "Business-stuff" tasks, such as comparing an Excel sheet with a PDF or another Excel sheet.
- Online presentations where you share one screen and use the other to see your notes.

These are typical workloads I have, where I also use two monitors, and I totally see the benefit. But to me, this is maybe 10% of the day.

## What's the Cost
Now, what's the cost when using multiple monitors? I currently see three cost factors:

**Initial cost**: The cost to buy a monitor. I don't think you should try to save a lot of money here since, in my opinion, a good monitor is crucial for a productive working day.

**Running cost**: Energy consumption. I think here you can and should actually save money. People often underestimate the running cost of a monitor, especially if they have higher refresh rates of 120Hz or more. So, if you are currently just using one of them, try turning off the other one for a moment. It will help your electricity bill, and it will help you with:

**Stress cost**: This is a very subjective thing, so I will write down my personal view here. Very often, having multiple monitors turned on while only using one of them causes me a lot of distraction and stress. This could be due to:
- Messages in Slack.
- Emails.
- News popping up.
- Other documents or code which I currently don't look at, but are still constantly on my mind because I see them in peripheral sight.

## My Setup
So, I'm currently running the 1.5 monitors setup everywhere. I would say that 90% of the day, the laptop lid is closed, and I only use one monitor. But for some workloads, I totally see the benefit of two monitors, and that's when I usually open the laptop lid and use it as a second screen.

To me, having two "real" screens always on was, in the end, more of a distraction and damaged my workflow than I had any benefits from. But I also don't like working with only one screen all the time, so I currently see the 1.5 monitor setup as perfect.

## Conclusion
In my observations of other people I work with and mentor, I see that some are really keeping up productivity with three monitors and also often use all three of them. Usually, those are people who also have better capabilities to multitask than I have. But I also mentored someone who told me that they felt distracted all the time and had difficulties focusing on their tasks. So I recommended to them what helped me: Stop multitasking. Try to split your workday into slots of X minutes, whatever fits for you, and just work on a single thing during that time. If you choose something like 20-30 minutes, then there's basically no message you can't wait to respond to until your slot ends. If you then choose to use only one monitor, you are also not distracted by any Slack messages on the second one. What I also use a lot is workspaces. By now, Linux, Mac, and Windows all support them. Usually, I have a cluttered workspace with mail, Slack, and other programs. And when I work with code, I am on a separate workspace.

I think this is a very subjective topic, and everybody should do what fits their needs the most. To me, it turned out that multiple monitors are often just too distracting. Sometimes for unnecessary "media stuff," but sometimes also the "work on idle" on the other screen is constantly putting jitter in my brain. That's why I ended up using just one monitor with the optional laptop screen if needed.
