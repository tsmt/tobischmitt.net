---
title: "Too much software"
date: 2024-05-27T22:00:00+02:00
draft: true
taxonomies:
  tags: ["mental health", "work", "stress", "open source"]
---

## Work produces work
